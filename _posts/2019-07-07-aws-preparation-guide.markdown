---
layout: post
title: "AWS Certified Solutions Architect."
date: 2019-07-07T13:38:01-08:00
---

**The Open Guide to Amazon Web Services:**

* [https://github.com/open-guides/og-aws](https://github.com/open-guides/og-aws)

**Книги:**

* [AWS Certified Solutions Architect Official Study Guide](https://www.pdfdrive.com/aws-certified-solutions-architect-official-study-guide-d60702917.html)

* [AWS Certified Solutions Architect Official Study Guide: Associate Exam](https://www.pdfdrive.com/aws-certified-solutions-architect-official-study-guide-associate-exam-d38558089.html)

**Udemy:**

* [AWS Certified Solutions Architect - Associate 2019](https://www.udemy.com/aws-certified-solutions-architect-associate/)

**Pluralsight:**

* [Demystifying the AWS Certified Solutions Architect: Associate Exam](https://app.pluralsight.com/library/courses/demystifying-aws-certified-solutions-architect-associate-exam/table-of-contents)
  
* [Amazon Web Services (AWS) Fundamentals for System Administrators](https://app.pluralsight.com/library/courses/aws-system-admin-fundamentals/table-of-contents)

**Mindmaps:**

* [AWS Certified Solutions Architect Associate](https://www.mindmeister.com/1179061789/aws-certified-solutions-architect-associate)

**Quizlet:**

[Aws solutions architect associate test questions flash cards](https://quizlet.com/123620854/aws-solutions-architect-associate-test-questions-flash-cards/)

[Aws flashcards (377)](https://quizlet.com/247634729/aws-flash-cards/)

[Aws flash cards (421)](https://quizlet.com/319813448/aws-flash-cards/)

[Ec2 flash cards](https://quizlet.com/306504568/ec2-flash-cards/)